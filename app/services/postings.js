angular.module('myApp.postings', ['ngStorage'])

.service('Postings', ['$localStorage', '$q', function($localStorage, $q) {
    this.savePosting = function(postingData) {
        var postings = $localStorage.postingsData || [];
        postings.push(postingData);
        $localStorage.postingsData = postings;

        return $q(function(resolve, reject) {
            if (typeof $localStorage.postingsData != 'undefined') {
                resolve('Your posting has been saved successfully');
            } else {
                reject('Cannot save your posting, sorry');
            }
        });
    }
}]);