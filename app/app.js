'use strict';

angular.module('myApp', [
    'ngRoute',
    'myApp.home',
    'myApp.addPosting',
    'myApp.postings',
    'myApp.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.otherwise({ redirectTo: '/home' });
}]);