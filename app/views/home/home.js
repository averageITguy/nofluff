'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {
        templateUrl: 'views/home/home.html',
        controller: 'HomeCtrl'
    });
}])

.controller('HomeCtrl', ['$scope', '$localStorage', function($scope, $localStorage) {

    $scope.postings = [];

    $scope.init = function() {
        $scope.postings = $localStorage.postingsData;
    }

    $scope.init();
}]);