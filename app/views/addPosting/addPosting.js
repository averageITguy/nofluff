'use strict';

angular.module('myApp.addPosting', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/addPosting', {
        templateUrl: 'views/addPosting/addPosting.html',
        controller: 'AddPostingCtrl'
    });
}])

.controller('AddPostingCtrl', ['$scope', 'Postings', '$location', '$timeout', function($scope, Postings, $location, $timeout) {

    $scope.showFormError = false;
    $scope.showFormSuccess = false;
    $scope.errorMessage = '';
    $scope.successMessage = '';
    $scope.addPostingForm = {
        title: '',
        company: '',
        salaryMin: null,
        salaryMax: null,
        streetName: '',
        city: '',
        postalCode: ''
    }

    $scope.resetAddPostingForm = function() {
        $scope.addPostingForm = {};
        $scope.addPostingForm.$valid = true;
        $scope.toggleFormError = false;
    };

    $scope.toggleFormError = function(msg) {
        if (!msg) {
            $scope.showFormError = false;
        } else {
            $scope.showFormError = true;
            $scope.errorMessage = msg;
        }
    }

    $scope.toggleFormSuccess = function(msg) {
        if (!msg) {
            $scope.showFormSuccess = false;
        } else {
            $scope.showFormSuccess = true;
            $scope.successMessage = msg;
        }
    }

    $scope.submitAddPostingForm = function() {
        $scope.toggleFormError(false);

        if ($scope.addPostingForm.$valid) {
            var postingModel = {
                title: $scope.addPostingForm.title,
                company: $scope.addPostingForm.company,
                salaryMin: $scope.addPostingForm.salaryMin,
                salaryMax: $scope.addPostingForm.salaryMax,
                streetName: $scope.addPostingForm.streetName,
                city: $scope.addPostingForm.city,
                postalCode: $scope.addPostingForm.postalCode
            }

            Postings.savePosting(postingModel).then(function(msg) {
                $scope.toggleFormSuccess(msg);
                $timeout(function() {
                    $location.path("/home");
                }, 2000);
            }, function(msg) {
                $scope.toggleFormError(msg);
            });
        } else {
            $scope.toggleFormError('Form is invalid, please check if all fields are filled properly');
        }
    };
}]);